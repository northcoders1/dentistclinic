import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DomService {

  constructor() { }

  rafAsync() {
    return new Promise(resolve => {
      requestAnimationFrame(resolve);
    });
  }

  checkElement(selector) {
    if (document.querySelector(selector) === null) {
      return this.rafAsync().then(() => this.checkElement(selector));
    } else {
      return Promise.resolve(true);
    }
  }

}
