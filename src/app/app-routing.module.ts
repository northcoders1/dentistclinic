import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MainComponent} from './components/main/main.component';
import {DepartmentsComponent} from './components/departments/departments.component';
import {FAQComponent} from './components/faq/faq.component';
import {ArticlesComponent} from './components/articles/articles.component';
import {FeedbackComponent} from './components/feedback/feedback.component';
import {SpecialistsComponent} from './components/specialists/specialists.component';

const routes: Routes = [
  { path: '', component: MainComponent },
  { path: 'departments', component: DepartmentsComponent },
  { path: 'faq', component: FAQComponent },
  { path: 'articles', component: ArticlesComponent},
  { path: 'feedback', component: FeedbackComponent },
  { path: 'specialists', component: SpecialistsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
