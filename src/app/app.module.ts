import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MainComponent } from './components/main/main.component';
import { DepartmentsComponent } from './components/departments/departments.component';
import { HeaderComponent } from './components/common/header/header.component';
import { FooterComponent } from './components/common/footer/footer.component';
import {AppRoutingModule} from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {
  MatCheckboxModule,
  MatButtonModule,
  MatToolbarModule,
  MatSidenavModule,
  MatListModule,
  MatIconModule,
  MatMenuModule,
  ErrorStateMatcher,
  ShowOnDirtyErrorStateMatcher,
  MatFormFieldModule,
  MatInputModule,
  MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule, MatGridListModule, MatTooltipModule, MatCardModule, MatExpansionModule
} from '@angular/material';
import { AppointmentComponent } from './components/common/header/appointment/appointment.component';
import {A11yModule} from '@angular/cdk/a11y';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FAQComponent } from './components/faq/faq.component';
import { ArticlesComponent } from './components/articles/articles.component';
import { FeedbackComponent } from './components/feedback/feedback.component';
import { SpecialistsComponent } from './components/specialists/specialists.component';


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    DepartmentsComponent,
    HeaderComponent,
    FooterComponent,
    AppointmentComponent,
    FAQComponent,
    ArticlesComponent,
    FeedbackComponent,
    SpecialistsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatMenuModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    A11yModule,
    FlexLayoutModule,
    MatGridListModule,
    MatTooltipModule,
    MatCardModule,
    MatExpansionModule
  ],
  entryComponents: [
    AppointmentComponent
  ],
  providers: [
    {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher},
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
