import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-specialists',
  templateUrl: './specialists.component.html',
  styleUrls: ['./specialists.component.css']
})
export class SpecialistsComponent implements OnInit {

  therapists: string[];
  therapistsSurgeons: string[];
  hygienists: string[];
  orthopedists: string[];
  childrensDentist: string[];
  implantSurgeons: string[];
  orthodontists: string[];

  constructor() { }

  ngOnInit() {
    this.therapists = [
      'Антонюк Наталья Валентиновна',
      'Мустафаева Ксения Сергеевна',
      'Мельникова Екатерина Петровна',
      'Потрохова Анна Станиславовна',
      'Разоренова Елена Владимировна'
    ];

    this.therapistsSurgeons = [
      'Дашкевич Роман Викторович',
      'Гапоник Инесса Борисовна',
      'Тарасова Анжелика Викторовна',
      'Долгих Иван Андреевич',
      'Романов Михаил Романович'
    ];

    this.hygienists = [
      'Магдыш Анна Игоревна',
      'Викторова Мария Евгеньевна',
      'Медвинская Станислава Валерьевна',
      'Белая Екатерина Геннадьевна'
    ];

    this.orthopedists = [
      'Юнусов Эдуард Мансурович',
      'Сахалтуева Александра Михайловна',
      'Кобелев Андрей Евгеньевич',
      'Исаков Игорь Олегович',
      'Николаев Николай Эдуардович',
      'Шмурун Дмитрий Александрович'
    ];

    this.childrensDentist = [
      'Беляева Регина Станиславовна',
      'Дашкевич Роман Викторович'
    ];

    this.implantSurgeons = [
      'Мирбакиев Дильмурат Рахманович'
    ];

    this.orthodontists = [
      'Мирзоева Алина Насимовна'
    ];
  }

}
