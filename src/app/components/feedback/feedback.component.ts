import { Component, OnInit } from '@angular/core';
import {FeedbackModel} from '../../models/feedbackModel';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {

  feedbackList: FeedbackModel[];

  constructor() { }

  ngOnInit() {
    this.feedbackList = [
      { name: 'Жмилевский Владислав Дмитриевич',
        timestamp: '13.12.2020',
        review: 'Очень хорошее обслуживание. Приятный интерьер и хорошие сотрудники.' +
          ' Приемлимые цены и прекрасное лечение. Рекомендую другим, у кого проблемы с зубами. ' +
          'Удобная регистрация на сайте и удобный интерфейс.' },
      {name: 'Давыдов Илья Сергеевич',
        timestamp: '12.12.2020',
        review: 'Очень понравилось посещать стоматологию! Хорошие цены и удобные часы работы! Удобное местопложение.' +
          ' Очень хорошо, что можно делать запись онлайн.'},
      {name: 'Хованский Юрий Михайлович',
        timestamp: '11.12.2020',
        review: 'Посещал стоматологию по определённому адресу. Было очень комфортно в зале ожидания. Лечение проходило потрясающе.' +
          ' Всем советую.'}
    ];
  }

}
