import {AfterViewInit, Component, OnInit} from '@angular/core';
import {DomService} from '../../services/dom.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit, AfterViewInit {

  constructor(public dom: DomService) {
  }

  title: string;
  bannerDescription: string;
  explanationTitle: string;
  listItems = [];
  cardExplanation: string;

  static resizeDivHeight() {
    let height;
    height = document.getElementById('explanationTitle').offsetHeight;
    document.getElementById('heightPlaceholder').style.height = (height * 2) + 'px';
  }

  ngOnInit() {
    /* Оставляется возможность для динамического баннера, скажем, если надо поправить описание,
      то можно получать данные из возможной админки (по API, достаётся из базы данных). Для прототипа используются
      захардкоденные плейсхолдерные значения.*/

    this.title = 'Специальные условия для тех, кому сложно сделать первый шаг';
    this.bannerDescription = 'Часто откладываете визит к стоматологу? Нет времени, боитесь потратить много денег или просто не можете принять решение? У нас есть для вас предложение!';
    this.explanationTitle = 'Получите бесплатную консультацию и специальные условия на первый визит';
    this.listItems = [
      'консультация по услугам и стоимости',
      'как снять зубную боль и что делать дальше',
      'гигиена и сохранение здоровья зубов',
      'безболезненное лечение',
      'имплантация и протезирование',
      'правильный прикус и внешний вид зубов',
      'отбеливание зубов и др. вопросы'
    ];
    this.cardExplanation = 'Отправьте заявку, и мы перезвоним вам в течение 5 минут';

    window.addEventListener('resize', MainComponent.resizeDivHeight);

  }

  ngAfterViewInit() {
    this.dom.checkElement('#explanationTitle')
      .then(() => {
        MainComponent.resizeDivHeight();
      });
  }
}
