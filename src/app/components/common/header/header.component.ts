import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material';
import {AppointmentComponent} from './appointment/appointment.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  openDialog() {
    this.dialog.closeAll();
    const dialogRef = this.dialog.open(AppointmentComponent);
  }
}


