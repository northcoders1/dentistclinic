export class FeedbackModel {
  name: string;
  timestamp: string;
  review: string;
}
